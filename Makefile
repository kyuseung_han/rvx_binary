# ****************************************************************************
# ****************************************************************************
# Copyright SoC Design Research Group, All rights reserved.
# Electronics and Telecommunications Research Institute (ETRI)
# 
# THESE DOCUMENTS CONTAIN CONFIDENTIAL INFORMATION AND KNOWLEDGE
# WHICH IS THE PROPERTY OF ETRI. NO PART OF THIS PUBLICATION IS
# TO BE USED FOR ANY OTHER PURPOSE, AND THESE ARE NOT TO BE
# REPRODUCED, COPIED, DISCLOSED, TRANSMITTED, STORED IN A RETRIEVAL
# SYSTEM OR TRANSLATED INTO ANY OTHER HUMAN OR COMPUTER LANGUAGE,
# IN ANY FORM, BY ANY MEANS, IN WHOLE OR IN PART, WITHOUT THE
# COMPLETE PRIOR WRITTEN PERMISSION OF ETRI.
# ****************************************************************************
# 2020-03-17
# Kyuseung Han (han@etri.re.kr)
# ****************************************************************************
# ****************************************************************************

include ../rvx_python_config.mh

ifndef RVX_UTIL_HOME
	RVX_UTIL_HOME=${CURDIR}/../rvx_util
endif

ORIGINAL_FILE_LIST=./.git ./.gitignore ./Makefile ./info ./rvx_setup.sh.template
include ${RVX_UTIL_HOME}/distclean.mh

config:
	${PYTHON3_CMD} ${RVX_UTIL_HOME}/configure_template.py -i ./rvx_setup.sh.template -o ./rvx_setup.sh -c RVX_BINARY_HOME=$(CURDIR)

compiler: compiler_basic

compiler_basic:
	${PYTHON3_CMD} ${RVX_UTIL_HOME}/get_binary.py -target compiler_basic -info ./info -output ./compiler_basic --removes_top_dir

compiler_sifive:
	${PYTHON3_CMD} ${RVX_UTIL_HOME}/get_binary.py -target compiler_sifive -info ./info -output ./compiler_sifive --removes_top_dir


.PHONY: compiler compiler_basic compiler_sifive
